from time import sleep
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.remote.webdriver import WebDriver

class MySeleniumTests(StaticLiveServerTestCase):
    port = 8000


    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver("http://selenium:4444", desired_capabilities={'browserName': 'chrome'})
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_login(self):
        self.selenium.get('%s:%s%s' % ('http://web', self.port, '/'))
        greeting = self.selenium.find_element_by_id("greeting")
        self.assertEqual(greeting.text, 'hello world')
